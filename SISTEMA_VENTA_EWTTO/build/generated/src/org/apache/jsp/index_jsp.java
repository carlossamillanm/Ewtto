package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("\n");
      out.write("        <title>Login</title>\n");
      out.write("\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n");
      out.write("\n");
      out.write("        <!-- Bootstrap -->\n");
      out.write("        <link href=\"assets/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->\n");
      out.write("        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->\n");
      out.write("        <!--[if lt IE 9]>\n");
      out.write("          <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n");
      out.write("          <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n");
      out.write("        <![endif]-->\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            html, body {\n");
      out.write("                height: 100%;\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 0;\n");
      out.write("                margin: 0;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #full-screen-background-image {\n");
      out.write("                z-index: -999;\n");
      out.write("                width: 100%;\n");
      out.write("                height: auto;\n");
      out.write("                position: fixed;\n");
      out.write("                top: 0;\n");
      out.write("                left: 0;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <img alt=\"full screen background image\" src=\"assets/imagenes/fondo.jpg\" id=\"full-screen-background-image\" /> \n");
      out.write("    <div class=\"panel panel-primary text-center\">\n");
      out.write("        <div class=\"panel-heading\">\n");
      out.write("            <h1><img src=\"assets/imagenes/Logo.jpg\" />  <span class=\"glyphicon glyphicon-align-center\"></span><FONT FACE=\"impact\" SIZE=7 COLOR=\"white\">  EMPRESA EWTTO </FONT>  <span class=\"glyphicon glyphicon-align-center\"></span>  <img src=\"assets/imagenes/Logo.jpg\" /></h1>\n");
      out.write("            <br>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <div class=\"container\">\n");
      out.write("        <div class=\"row\">\n");
      out.write("            <div class=\"col-md-3\"></div>\n");
      out.write("            <div class=\"col-md-6\">\n");
      out.write("                <div class=\"panel panel-default\">\n");
      out.write("                    <div class=\"text-center\">\n");
      out.write("                        <H1>Acceso al Sistema</H1>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"panel-body\">\n");
      out.write("\n");
      out.write("                        <form name=\"sLogin\" id=\"sLogin\" action=\"sLogin\" method=\"POST\">\n");
      out.write("                            <div class=\"form-group col-md-5\">\n");
      out.write("                                <div class=\"slider\">\n");
      out.write("\n");
      out.write("                                    <a><img src=\"assets/imagenes/login.png\" width=\"200px\" height=\"200\"  ></a>\n");
      out.write("\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"form-group col-md-7\" style=\"border-left:1px solid #ccc;height:200px\">\n");
      out.write("\n");
      out.write("                                <div>\n");
      out.write("                                    <label>Usuario</label>\n");
      out.write("                                    <div class=\"input-group\">\n");
      out.write("                                        <span class=\"input-group-addon\" id=\"basic-addon1\"><span class=\"glyphicon glyphicon-user\"></span></span>\n");
      out.write("                                        <input type=\"text\" name=\"usuario\" id=\"usuario\" class=\"form-control\" placeholder=\"Usuario\" aria-describedby=\"basic-addon1\" >\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <p><br></p>\n");
      out.write("                                <div>\n");
      out.write("                                    <label>Contraseña</label>\n");
      out.write("                                    <div class=\"input-group\">\n");
      out.write("                                        <span class=\"input-group-addon\" id=\"basic-addon1\"><span class=\"glyphicon glyphicon-lock\"></span></span>\n");
      out.write("                                        <input type=\"password\" name=\"clave\" id=\"clave\" class=\"form-control\" placeholder=\"Contraseña\" aria-describedby=\"basic-addon1\">\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <p><br></p>\n");
      out.write("                                <div class=\"form-group col-md-12\">\n");
      out.write("                                    <div class=\"text-center\"> \n");
      out.write("                                        <button type=\"submit\" value=\"Ingresar\" name=\"opcion\" id=\"opcion\" class=\"btn btn-success\" ><span class=\"glyphicon glyphicon-ok\"></span> Ingresar </button>\n");
      out.write("\n");
      out.write("                                        <button type=\"submit\" value=\"Cancelar\" name=\"opcion\" id=\"opcion\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-remove\"></span> Cancelar</button>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </form>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
