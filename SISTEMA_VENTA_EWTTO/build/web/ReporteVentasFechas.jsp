<%@page import="java.sql.Date"%>
<%@page import="ConexionBD.CADO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="java.util.*" %> 
<%@ page import="java.io.*" %> 
<%@ page import="java.sql.*" %> 
<%
   CADO ocado = new CADO();
    File reportFile = new File(application.getRealPath("//Reportes//Ventas_Fechas_1.jasper"));
    Map parameters = new HashMap();
    String valor1=request.getParameter("fechainicio");
    Date a1= Date.valueOf(valor1);
    String valor2=request.getParameter("fechafin");
    Date a2= Date.valueOf(valor2);
    parameters.put("Inicio", a1);
    parameters.put("Fin", a2);
    byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, ocado.Conexion());
    response.setContentType("application/pdf");
    response.setContentLength(bytes.length);
    ServletOutputStream ouputStream = response.getOutputStream();
    ouputStream.write(bytes, 0, bytes.length);
    ouputStream.flush();
    ouputStream.close();
%>