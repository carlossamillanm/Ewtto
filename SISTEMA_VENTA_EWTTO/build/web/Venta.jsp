<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Cliente" %>
<%@page import="Beans.Producto" %>
<jsp:include page="sVenta?opcion=ListarClientes" />
<jsp:include page="sVenta?opcion=listarProducto" />
<jsp:useBean id="listacliente" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="listaAsignado" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="listapro" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ventas</title>

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 

            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="container">
                    <p><br></p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="page-header text-center">
                                        <h1>Registro de Ventas</h1>
                                    </div>
                                    <form name="sVenta" id="sVenta" action="sVenta" method="POST">
                                        <div>
                                            <div>
                                                <label for="exampleInputEmail1">Cliente</label>
                                                <div class="input-group col-md-9">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large" ></span></span>
                                                    <input type="text" class="form-control" name="nombre" 
                                                           value="<%if (listaAsignado.size() != 0) {
                                                                   Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                                   out.println(comprador.getNombre() + " " + comprador.getApellidoPaterno() + " " + comprador.getApellidoMaterno());
                                                               } else {
                                                                   out.println(" ");
                                                               }%>" id="nombre" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="text-center" >
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#buscar">
                                            Buscar Cliente
                                        </button>                                        
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registrar">
                                            Registrar Cliente
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="container">
                <p><br></p>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form name="sVenta" id="sVenta" action="sVenta" method="POST">
                                    <div class="text-center" >
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar">
                                            Agregar Producto
                                        </button> 
                                    </div>
                                    <p><br></p>
                                </form>
                                <form name="sVenta" id="sVenta" action="sVenta" method="get">
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" name="cli" id="cliente" value="<%if (listaAsignado.size() != 0) {
                                                Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                out.print(comprador.getDni());
                                            } else {
                                                out.print(1);
                                            }%>">
                                    </div>
                                    <table class="table table-bordered col-md-9">
                                        <tbody>
                                            <tr>
                                                <td colspan="3" align="center">   Numero del documento   </td>
                                                <td colspan="3" align="center"><input type="text" class="form-control" name="num" id="num" placeholder="Numero del documento" aria-describedby="basic-addon1" required="required"></td>
                                                <td colspan="3" align="center">   Tipo de Documento   </td>
                                                <td colspan="3" align="center"><input type="radio" class="form-group" value="F" name="tipo" id="f" aria-describedby="basic-addon1">Factura        <input type="radio" class="form-group" value="B" name="tipo" id="b" aria-describedby="basic-addon1">Boleta   <input type="radio" class="form-group" value="N" name="tipo" id="n" aria-describedby="basic-addon1">Nota de Venta</td>
                                            </tr>
                                            <tr>
                                                <td align="center">     Codigo   </td>
                                                <td align="center">       Nombre     </td>
                                                <td align="center">       Marca      </td>
                                                <td align="center">     P.por mayor </td>
                                                <td align="center">     P.por Menor </td>
                                                <td align="center">   P.por Factura </td>
                                                <td align="center">     P.Caja      </td>
                                                <td align="center">     Stock      </td>
                                                <td align="center">   Precio      </td>
                                                <td align="center">   Cantidad    </td>
                                                <td align="center">    </td>
                                            </tr>  

                                            <%
                                                ArrayList<Producto> lProd = (ArrayList<Producto>) session.getAttribute("carrito");
                                                if (lProd != null) {
                                                    for (int i = 0; i < lProd.size(); i++) {
                                                        Producto pro = (Producto) lProd.get(i);
                                            %>
                                            <tr><td><% out.println(pro.getCodigo()); %></td>
                                                <td><% out.println(pro.getDescripcion().toUpperCase()); %></td>
                                                <td><% out.println(pro.getMarca().toUpperCase()); %></td>
                                                <td><% out.println(pro.getPrecioMayor()); %></td>
                                                <td><% out.println(pro.getPrecioMenor()); %></td>
                                                <td><% out.println(pro.getPrecioFactura()); %></td>
                                                <td><% out.println(pro.getPrecioCaja()); %></td>
                                                <td><% out.println(pro.getStock()); %></td>
                                                <td><input type="text" class="form-control" name="Precio<%out.print(i);%>" id="precio" placeholder="Precio" aria-describedby="basic-addon1" required="required"></td>
                                                <td><input type="text" class="form-control" name="Cantidad<%out.print(i);%>" id="cantidad" placeholder="Cantidad" aria-describedby="basic-addon1" required="required"></td>
                                                <td align="center">
                                                    <a href="sVenta?opcion=RemoverCarrito&pro_id=<%out.println(pro.getId());%>&cli=<%if (listaAsignado.size() != 0) {
                                                            Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                            out.println(comprador.getDni());
                                                        } else {
                                                            out.println(1);
                                                        }%>"><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                            </tr>
                                            <%}
                                            }%>
                                        </tbody>
                                    </table>
                                    <div class="text-center" >
                                        <button type="submit" class="btn btn-primary" value="Venta" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Generar Venta </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="registrar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detalle de Constancia</h4>
                    </div>
                    <div class="modal-body">
                        <form name="sVenta" id="sVenta" action="sVenta" method="POST">
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Nombre</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Apellido Paterno</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                    <input type="text" class="form-control" name="apaterno" id="apaterno" placeholder="Apellido Paterno" aria-describedby="basic-addon1" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Apellido Materno</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                    <input type="text" class="form-control" name="amaterno" id="amaterno" placeholder="Apellido Materno" aria-describedby="basic-addon1" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Direccion</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-home"></span></span>
                                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" aria-describedby="basic-addon1" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">telefono</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone"></span></span>
                                    <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Telefono" aria-describedby="basic-addon1" maxlength="9" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">DNI</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                    <input type="text" class="form-control" name="dni" id="dni" placeholder="DNI" aria-describedby="basic-addon1" maxlength="8" required="required">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">RUC</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                    <input type="text" class="form-control" name="Ruc" id="Ruc" placeholder="RUC" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success" value="Registrar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Registrar </button>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="buscar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detalle de Constancia</h4>
                    </div>
                    <div class="modal-body">
                        <%-- <form name="sVenta" id="sVenta" action="sVenta" method="POST">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Buscar Cliente</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                        <input type="text" class="form-control" name="dni" id="dni" placeholder="Dni" aria-describedby="basic-addon1" required="required">
                                    </div>
                                </div>
                                 <div class="text-center" >
                                    <button type="submit" class="btn btn-primary" value="Buscar" name="opcion" id="opcion"><span class="glyphicon glyphicon-search"></span> Buscar </button>
                                </div>
                            </form> --%>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td align="center">Id</td>
                                    <td align="center">Nombre</td>
                                    <td align="center">Apellido Paternoe</td>
                                    <td align="center">Apellido Materno</td>
                                    <td align="center">DNI</td>
                                    <td align="center">Telefono</td>
                                    <td align="center">Operaciones</td>
                                </tr>                  
                                <%
                                    if (listacliente != null) {
                                        for (int i = 0; i < listacliente.size(); i++) {
                                            Cliente cli = (Cliente) listacliente.get(i);
                                %>
                                <tr><td><% out.println(cli.getId()); %></td>
                                    <td><% out.println(cli.getNombre()); %></td>
                                    <td><% out.println(cli.getApellidoPaterno()); %></td>
                                    <td><% out.println(cli.getApellidoMaterno()); %></td>
                                    <td><% out.println(cli.getDni()); %></td>
                                    <td><% out.println(cli.getTelefono()); %></td>
                                    <td>
                                        <a href="sVenta?opcion=Agregar&cli_id=<%out.println(cli.getId());%>&cli_nom=<%out.println(cli.getNombre());%>&cli_apat=<%out.println(cli.getApellidoPaterno());%>&cli_amat=<%out.println(cli.getApellidoMaterno());%>&cli_tel=<%out.println(cli.getTelefono());%>&cli_dni=<%out.println(cli.getDni());%>&cli_Ruc=<%out.println(cli.getRuc());%>&cli_dir=<%out.println(cli.getDireccion());%>"><button class="btn btn-block btn-info btn-sm">Agregar</button></a>
                                    </td>
                                    <%}
                                        }%>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="agregar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Listado de Productos</h4>
                    </div>

                    <div class="modal-body">
                        <br>
                        <div class="box">
                            <center>
                                <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                            </center>
                        </div>
                        <div class="box-body">
                            <br>
                            <table class="order-table table">
                                <thead>
                                    <tr>
                                        <td align="center">   Id   </td>
                                        <td align="center">     Codigo      </td>
                                        <td align="center">     Nombre      </td>
                                        <td align="center">     Marca       </td>
                                        <td align="center">     Precio      </td>
                                        <td align="center">    </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        if (listapro != null) {

                                            for (int i = 0; i < listapro.size(); i++) {
                                                Producto pro = (Producto) listapro.get(i);
                                    %>

                                    <tr><td><% out.println(pro.getId()); %></td>
                                        <td><% out.println(pro.getCodigo()); %></td>
                                        <td><% out.println(pro.getDescripcion().toUpperCase()); %></td>
                                        <td><% out.println(pro.getMarca().toUpperCase()); %></td>
                                        <td><% out.println(pro.getPrecioMenor()); %></td>
                                        <td align="center">
                                            <a href="sVenta?opcion=Carrito&prod_id=<%out.println(pro.getId());%>&cli=<%if (listaAsignado.size() != 0) {
                                                    Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                    out.println(comprador.getDni());
                                                } else {
                                                    out.println(1);
                                                }%>"><span class="glyphicon glyphicon-plus"></span></a>
                                        </td>
                                        <%}
                                            }%>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>