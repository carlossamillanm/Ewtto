<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Cliente" %>
<jsp:include page="sCliente?opcion=ListarClientes" />
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Menu</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>{
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>

            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">

                <div class="col-md-12 well admin-content" >
                    <div> 
                        <a data-toggle="tab" href="#new_cliente"><button type="submit" value=""  class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Registrar Cliente</button></a>

                        <a data-toggle="tab" href="#listado"><button type="submit" value=""  class=" btn btn-default "  ><span class="glyphicon glyphicon-print"></span> Lista de Clientes </button></a>
                    </div> 
                </div>
                <div class="tab-content">
                    <div id="listado" class="tab-pane fade in active">
                        <div class="box">
                            <div class="box-header" align="center">
                                <h3 class="box-title">Clientes</h3>
                            </div><!-- /.box-header -->
                            <br>
                            <div class="box">
                                <center>
                                    <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                                </center>
                            </div>
                            <br>
                            <div class="box-body">
                                <table class="order-table table">
                                    <thead>
                                        <tr>
                                            <td align="center">Id</td>
                                            <td align="center">Nombre</td>
                                            <td align="center">Apellido Paternoe</td>
                                            <td align="center">Apellido Materno</td>
                                            <td align="center">DNI</td>
                                            <td align="center">RUC</td>
                                            <td align="center">Telefono</td>
                                            <td colspan="2" align="center">Operaciones</td>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <%
                                        if (lista != null) {
                                            for (int i = 0; i < lista.size(); i++) {
                                                Cliente cli = (Cliente) lista.get(i);
                                    %>
                                    <tr><td><% out.println(i + 1); %></td>
                                        <td><% out.println(cli.getNombre()); %></td>
                                        <td><% out.println(cli.getApellidoPaterno()); %></td>
                                        <td><% out.println(cli.getApellidoMaterno()); %></td>
                                        <td><% out.println(cli.getDni()); %></td>
                                        <td><% out.println(cli.getRuc()); %></td>
                                        <td><% out.println(cli.getTelefono()); %></td>
                                        <td>
                                            <a href="ModificarCliente.jsp?cli_id=<%out.println(cli.getId());%>&cli_nom=<%out.println(cli.getNombre());%>&cli_apat=<%out.println(cli.getApellidoPaterno());%>&cli_amat=<%out.println(cli.getApellidoMaterno());%>&cli_tel=<%out.println(cli.getTelefono());%>&cli_dni=<%out.println(cli.getDni());%>&cli_Ruc=<%out.println(cli.getRuc());%>&cli_dir=<%out.println(cli.getDireccion());%>"><button class="btn btn-block btn-info btn-sm">Modificar</button></a>
                                        </td>
                                        <td>
                                            <a href="sCliente?opcion=Eliminar&cli_id=<%out.println(cli.getId());%>"><button class="btn btn-block btn-danger btn-sm" value="Eliminar" name="opcion">Eliminar</button></a>
                                        </td>
                                        <%}
                                            }%>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /.box-body --> 
                    </div>    
                </div>
                <div id="new_cliente" class="tab-pane fade">
                    <div class="container">
                        <p><br></p>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="page-header text-center">
                                            <h1>Registro de Cliente</h1>
                                        </div>
                                        <form name="sCliente" id="sCliente" action="sCliente" method="POST">

                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Nombre</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Telefono</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone"></span></span>
                                                    <input type="text" class="form-control" name="telefono" id="telefono"  placeholder="Telefono" aria-describedby="basic-addon1" maxlength="9" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Apellido Paterno</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                    <input type="text" class="form-control" name="apaterno" id="apaterno" placeholder="Apellido Paterno" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Direccion</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-home"></span></span>
                                                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Apellido Materno</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                    <input type="text" class="form-control" name="amaterno" id="amaterno" placeholder="Apellido Materno" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">DNI</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                    <input type="text" class="form-control" name="dni" id="dni" placeholder="DNI" maxlength="8" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">RUC</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                    <input type="text" class="form-control" name="Ruc" id="Ruc" placeholder="RUC" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="text-center" >
                                                <button type="submit" class="btn btn-primary" value="Registrar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Registrar </button>
                                                <button type="submit" class="btn btn-danger" value="Cancelar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
