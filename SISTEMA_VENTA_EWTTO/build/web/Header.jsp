<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Menu</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="assets/css/estilos.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/fonts.css">
        <script src="assets/js/main.js"></script>

    </head>
    <body>
        <%
            ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
            if (user != null) {
                Usuario us = (Usuario) user.get(0);
        %> 
        <header>
            <div class="menu_bar ">
                <a href="#" class="bt-menu"><span class="icon-menu"></span>Menú</a>
            </div>

            <nav >
                <a  class="navbar-brand  " >EMPRESA EWTTO</a>
                <ul  >
                    <li><a href="MenuPrincipal.jsp"><span class="icon-home"></span>Inicio</a></li>
                    <li class="submenu" >
                        <%if (user.get(0).getTipo().equals("A")) {%>
                        <a href="#"><span class="icon-rocket"></span>Reportes<span class="caret icon-arrow"></span></a>
                        <ul class="children">
                            
                            <li><a href="ReporteProductosTotal.jsp" target="_blank"> Productos</a></li>
                            <li><a href="InformeVentas.jsp" target="_blank">Informe de Ventas</a></li>
                            <li><a href="ReporteClientes.jsp" target="_blank">Clientes</a></li>
                            <li><a href="ReporteUsuario.jsp"target="_blank">Usuarios</a></li>
                            <li><a href="BusquedaProductos.jsp">Productos por Venta</a></li>
                            <li><a href="Ventas_Fecha.jsp">Ventas por Fechas</a></li>
                            <li><a href="Compras.jsp">Productos por Compra</a></li>
                            <li><a href="ComprasFecha.jsp" target="_blank">Compras por Fechas</a></li>
                            <li><a href="ClientesFechas.jsp" target="_blank">Mejores Clientes</a></li>
                            <li><a href="UsuarioFechas.jsp" target="_blank">Mejores Vendedores</a></li> 
                        </ul>
                         <%}%>
                    </li>
                    <li><a href="#"><span class="icon-profile"></span><%out.println(us.getNombre() + " " + us.getApellidoPaterno() + " " + us.getApellidoMaterno());%></a>
                        <ul class="children">
                            <li><a href="sLogin?opcion=Cerrar">Cerrar Sesion</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>