<%@page import="Beans.Usuario"%>
<%@page import="Beans.DetalleVenta"%>
<%@page import="Beans.Producto"%>
<%@page import="Beans.Cliente" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="listaAsignado" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ventas</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="container">
                    <p><br></p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="page-header text-center">
                                        <h1>Registro de Ventas</h1>
                                    </div>

                                    <table class="table table-bordered col-md-9">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" align="center">   Cliente   </td>
                                                <td colspan="5" align="center">   <%if (listaAsignado.size() != 0) {
                                                        Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                        out.println(comprador.getNombre() + " " + comprador.getApellidoPaterno() + " " + comprador.getApellidoMaterno());
                                                    } else {
                                                        out.println(" ");
                                                    }%>  </td>
                                        </tr>
                                        <tr>
                                            <td align="center">   Id   </td>
                                            <td align="center">     Codigo      </td>
                                            <td align="center">     Nombre      </td>
                                            <td align="center">     Marca       </td>
                                            <td align="center">     Precio      </td>
                                            <td align="center">     Cantidad    </td>
                                            <td align="center">     Monto       </td>
                                        </tr>                  
                                        <%
                                            ArrayList<Producto> lProd = (ArrayList<Producto>) session.getAttribute("carrito");
                                            int con;
                                            if (lProd != null && lista != null) {
                                                for (int i = 0; i < lProd.size(); i++) {
                                                    DetalleVenta det = (DetalleVenta) lista.get(i);
                                                    Producto pro = (Producto) lProd.get(i);
                                                    Double a = det.getCantidad() * (Double) det.getPrecio();
                                        %>
                                        <tr><td align="center"><% out.println(i+1); %></td>
                                            <td align="center"><% out.println(pro.getCodigo()); %></td>
                                            <td align="center"><% out.println(pro.getDescripcion().toUpperCase()); %></td>
                                            <td align="center"><% out.println(pro.getMarca().toUpperCase()); %></td>
                                            <td align="center"><% out.println(det.getPrecio()); %></td>
                                            <td align="center"><% out.println(det.getCantidad()); %></td>
                                            <td align="center"><% out.println(a);%></td>
                                            <%}
                                                }%>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="center">TOTAL</td>
                                            <td align="center"><%DetalleVenta det = (DetalleVenta) lista.get(0);
                                                    out.println(det.getVenta().getTotal()); %></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group col-md-6">
                                    <a href="sVenta?opcion=Generar&validar=n&vt=<%out.println(det.getVenta().getTotal());%>&tipo=<%out.println(det.getVenta().getDocumento());%>&cod=<%out.print(det.getVenta().getNumero());
                                        for (int i = 0; i < lProd.size(); i++) {
                                            det = (DetalleVenta) lista.get(i);
                                            out.print("&cant" + i + "=" + det.getCantidad());
                                            out.print("&pre" + i + "=" + det.getPrecio());
                                        }%>&cli=<%if (listaAsignado.size() != 0) {
                                                Cliente comprador = (Cliente) listaAsignado.get(listaAsignado.size() - 1);
                                                out.println(comprador.getId());
                                            } else {
                                                out.println(1);
                                            }%>"><button class="btn btn-block btn-info btn-sm" value="Generar" name="opcion">Grabar venta</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
