<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registar Categoria</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>


            <section  class="posts col-md-10" style="background: lightgrey">
                <br>

                <div class="form-group col-md-15" style="border-left:1px solid #ccc;height:200px" >
                    <!--<div class="container-fluid" -->
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-heading text-center">
                                        <h1>Modificar Categoria</h1>
                                    </div>
                                    <div>
                                        <form name="sCategorias" id="sCategorias" action="sCategorias" method="POST">
                                            <div>
                                                <div>
                                                    <label for="exampleInputEmail1">Id</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                        <input type="text" class="form-control" name="id" id="id" value="<%out.println(request.getParameter("cat_id"));%>" placeholder="id" aria-describedby="basic-addon1" required="required" readonly>
                                                </div>
                                            </div>
                                            <div>
                                                <label for="exampleInputEmail1">Nombre</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" value="<%out.println(request.getParameter("cat_nom"));%>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div>
                                            <div class="text-center" >
                                                <button type="submit" class="btn btn-danger" value="Modificar" name="opcion"><span class="glyphicon glyphicon-remove"></span> Modificar </button>
                                                <button type="submit" class="btn btn-primary" value="Regresar" name="opcion"><span class="glyphicon glyphicon-pencil"></span> Regresar </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
             response.sendRedirect("index.jsp");
         }%> 
</html>
