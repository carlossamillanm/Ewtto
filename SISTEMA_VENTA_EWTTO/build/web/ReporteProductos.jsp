<%@page import="ConexionBD.CADO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="java.util.*" %> 
<%@ page import="java.io.*" %> 
<%@ page import="java.sql.*" %> 
<%
    CADO ocado = new CADO();
    File reportFile = new File(application.getRealPath("//Reportes//Producto.jasper"));
    Map parameters = new HashMap();
    parameters.put("Nombre_parametro", "Valor_Parametro");
    byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, ocado.Conexion());
    response.setContentType("application/pdf");
    response.setContentLength(bytes.length);
    ServletOutputStream ouputStream = response.getOutputStream();
    ouputStream.write(bytes, 0, bytes.length);
    ouputStream.flush();
    ouputStream.close();
%>