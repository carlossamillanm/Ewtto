<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Beans.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Categoria" %>
<jsp:include page="sCategorias?opcion=LlenarCombobox" />
<jsp:include page="sProducto?opcion=listarProducto" />
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="listapro" scope="request" class="java.util.ArrayList"/>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Productos</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 

            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">

                <div class="col-md-12 well admin-content" >
                    <div> 
                        <a data-toggle="tab" href="#new_producto"><button type="submit" value=""  class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Registrar Producto</button></a>

                        <a href="ReporteProductos.jsp" target="_blank"><button type="submit" value=""  class=" btn btn-default "  ><span class="glyphicon glyphicon-print"></span> Reporte Productos </button></a>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="listado" class="tab-pane fade in active">
                        <div class="box">
                            <div class="box-header" align="center">
                                <h3 class="box-title">Lista de Productos</h3>
                            </div><!-- /.box-header -->
                            <br>
                            <div class="box">
                                <center>
                                    <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                                </center>
                            </div>
                            <div class="box-body">
                                <br>
                                <table class="order-table table">
                                    <thead>
                                        <tr>
                                            <td align="center">Codigo</td>
                                            <td align="center">Descripcion</td>
                                            <td align="center">Marca</td>
                                            <td align="center">Precio al por Menor</td>
                                            <td align="center">Precio al por Mayor</td>
                                            <td align="center">Precio por Caja</td>
                                            <td align="center">Stock</td>
                                            <td align="center">Categoria</td>
                                            <td align="center">Imagen</td>
                                            <td colspan="2" align="center">Operaciones</td>
                                        </tr>  
                                    </thead>
                                    <tbody>
                                    <%
                                        if (listapro != null) {
                                            for (int i = 0; i < listapro.size(); i++) {
                                                Producto pro = (Producto) listapro.get(i);
                                    %>
                                    <tr><td align="center"><% out.println(pro.getCodigo()); %></td>
                                        <td align="center"><% out.println(pro.getDescripcion()); %></td>
                                        <td align="center"><% out.println(pro.getMarca()); %></td>
                                        <td align="center"><% out.println(pro.getPrecioMenor()); %></td>
                                        <td align="center"><% out.println(pro.getPrecioMayor()); %></td>
                                        <td align="center"><% out.println(pro.getPrecioCaja()); %></td>
                                        <td align="center"><% out.println(pro.getStock()); %></td>
                                        <td align="center"><% out.println(pro.getCateg().getNombre()); %></td>
                                        <td align="center"><img src="<% out.println(pro.getImagen());%>" alt=""></td>
                                        <td>
                                            <a href="ModificarProducto.jsp?pro_id=<%out.println(pro.getId());%>&pro_cod=<%out.println(pro.getCodigo());%>&pro_des=<%out.println(pro.getDescripcion());%>&pro_mar=<%out.println(pro.getMarca());%>&pro_pmn=<%out.println(pro.getPrecioMenor());%>&pro_pmy=<%out.println(pro.getPrecioMayor());%>&pro_caj=<%out.println(pro.getPrecioCaja());%>&pro_fact=<%out.println(pro.getPrecioFactura());%>&pro_cos=<%out.println(pro.getPrecioCosto());%>&pro_stk=<%out.println(pro.getStock());%>"><button class="btn btn-block btn-info btn-sm">Modificar</button></a>
                                        </td>
                                        <td>
                                            <a href="sProducto?opcion=Eliminar&pro_id=<%out.println(pro.getId());%>"><button class="btn btn-block btn-danger btn-sm" value="Eliminar" name="opcion">Eliminar</button></a>
                                        </td>
                                        <%}
                                            }%>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /.box-body --> 
                    </div>    
                </div>
                <div id="new_producto" class="tab-pane fade">
                    <div class="container">
                        <p><br></p>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="page-header text-center">
                                            <h1>Registro de Producto</h1>
                                        </div>
                                        <form name="sProducto" id="sProducto" action="sProducto" method="POST">
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Codigo</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                    <input type="text" class="form-control" name="codigo" maxlength="50" id="codigo" placeholder="Codigo" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Descripcion</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                    <input type="text" class="form-control" name="nombre" maxlength="100" id="nombre" placeholder="Descripcion" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Marca</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                    <input type="text" class="form-control" name="marca" maxlength="50" id="marca" placeholder="Marca" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Precio por Menor</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                    <input type="text" class="form-control" name="preciomenor" id="preciomenor" placeholder="Precio por Menor" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Precio por Mayor</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                    <input type="text" class="form-control" name="preciomayor" id="preciomayor" placeholder="Precio por Mayor" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Precio por Caja</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                    <input type="text" class="form-control" name="preciocaja" id="preciomayor" placeholder="Precio por Caja" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Precio por Factura</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                    <input type="text" class="form-control" name="preciofactura" id="preciomayor" placeholder="Precio de Factura" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Precio por Costo</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                    <input type="text" class="form-control" name="preciocosto" id="preciocosto" placeholder="Precio de Costo" aria-describedby="basic-addon1" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Stock</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-minus"></span></span>
                                                    <input type="text" class="form-control" name="stock" id="stock" placeholder="Stock" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Imagen</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-file"></span></span>
                                                    <input type="file" class="form-inline" name="imagen" id="imagen"  aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Categoria</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-list"></span></span>
                                                    <select class="form-control" name="categoria" id="categoria" aria-describedby="basic-addon1">
                                                            <%
                                                                for (int i = 0; i < lista.size(); i++) {
                                                                    Categoria cat = (Categoria) lista.get(i);
                                                            %>
                                                            <option><% out.println(cat.getNombre());
                                                            }%></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="text-center" >
                                                <button type="submit" class="btn btn-primary" value="Registrar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Registrar </button>
                                                <button type="submit" class="btn btn-danger" value="Cancelar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
