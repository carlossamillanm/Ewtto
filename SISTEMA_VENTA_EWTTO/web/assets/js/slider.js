/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Almacena slider en variable
var slider=$('#slider');

//botones
var siguiente=$('#btn-next');
var anterior=$('#btn-prev');

//mostrar ultima al primer lugar
$('#slider section:last').insertBefore('#slider section:first');

//mostrar la primera imagen con un margen de -100%

slider.css('margin-left', '-' + 100+'%');


function  moverD(){
    slider.animate({marginleft:'-'+200+'%'},700,
    function (){
        $('#slider section:first').insertAfter('#slider section:last');
        slider.css('margin-left','-'+100+'%');
    });
}

function  moverI(){
    slider.animate({marginleft:0},700,
    function (){
        $('#slider section:last').insertAfter('#slider section:first');
        slider.css('margin-left','-'+100+'%');
    });
}

siguiente.on('click',function() {
    moverD();
});
anterior.on('click',function() {
    moverI();
});

function autoplay(){
    interval=setInterval(function(){
        moverD();
        
    },5000);
}

autoplay();
