<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Menu</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <link href="assets/bootstrap/css/datepicker.css" rel="stylesheet">
        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script src="assets/bootstrap/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker();

            });
        </script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>

            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="tab-content">
                    <div class="box-header" align="center">
                        <h3 class="box-title">Buscar Clientes en fechas</h3>
                    </div><!-- /.box-header -->
                    <br>
                    <div class="panel-body">
                        <form name="ReporteClienteFechas" id="ReporteClienteFechas" action="ReporteClienteFechas.jsp" method="post">
                            <div class="form-group col-md-8">
                                <label for="exampleInputEmail1">Fecha Inicio</label>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span><input type='date' name="fechainicio" id="fecha" class="form-control" />        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="exampleInputEmail1">Fecha de Fin</label>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker'>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span><input type='date' name="fechafin" id="fecha" class="form-control" />        
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-primary" value="Buscar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Generar Reporte </button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
