<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="sCategorias?opcion=listar" />
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<%@page import="Beans.Categoria" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Categorias</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
         <script src="assets/js/BuscarTabla.js"></script>
    </head>
     <%
            ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
            if (user != null) {
        %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>

            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">

                <div class="col-md-12 well admin-content" >
                    <div> 
                        <a data-toggle="tab" href="#new_categoria"><button type="submit" value=""  class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Registrar Categoria</button></a>

                        <a data-toggle="tab" href="#listado"><button type="submit" value=""  class=" btn btn-default "  ><span class="glyphicon glyphicon-print"></span> Reporte Categoria </button></a>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="listado" class="tab-pane fade in active">
                        <div class="box">
                            <div class="box-header" align="center">
                                <h3 class="box-title">Categorias</h3>
                            </div><!-- /.box-header -->
                            <br>
                            <div class="box">
                                <center>
                                    <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                                </center>
                            </div>
                            <br>
                            <div class="box-body">
                                <table class="order-table table">
                                    <tbody>
                                        <tr>
                                            <td align="center">Id</td>
                                            <td align="center">Nombre</td>
                                            <td colspan="2" align="center">Operaciones</td>
                                        </tr>                  
                                    <%
                                        if (lista != null) {
                                            for (int i = 0; i < lista.size(); i++) {
                                                Categoria cat = (Categoria) lista.get(i);
                                    %>
                                    <tr><td><% out.println(i+1); %></td>
                                        <td><% out.println(cat.getNombre()); %></td>
                                        <td>
                                            <a href="ModificarCategoria.jsp?cat_id=<%out.println(cat.getId());%>&cat_nom=<%out.println(cat.getNombre());%>"><button class="btn btn-block btn-info btn-sm">Modificar</button></a>
                                        </td>
                                        <td>
                                            <a href="sCategorias?opcion=Eliminar&cat_id=<%out.println(cat.getId());%>"><button class="btn btn-block btn-danger btn-sm" value="Eliminar" name="opcion">Eliminar</button></a>
                                        </td>
                                        <%}
                                                }%>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /.box-body --> 
                    </div>    
                </div>
                <div id="new_categoria" class="tab-pane fade">
                    <div class="container">
                        <p><br></p>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="page-header text-center">
                                            <h1>Registro de Categoria</h1>
                                        </div>

                                        <form name="sCategorias" id="sCategorias" action="sCategorias" method="POST">
                                            <div>
                                                <div>
                                                    <label for="exampleInputEmail1">Nombre</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div>
                                                <div class="text-center" >
                                                   
                                                    <button type="submit" class="btn btn-primary" value="Registrar" name="opcion"><span class="glyphicon glyphicon-pencil"></span> Registrar </button>
                                                     <button type="submit" class="btn btn-danger" value="Cancelar" name="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
                response.sendRedirect("index.jsp");
            }%> 
</html>
