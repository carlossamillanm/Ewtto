<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Usuario" %>
<jsp:include page="sUsuario?opcion=ListarUsuario" />
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Usuario</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">

                <div class="col-md-12 well admin-content" >

                    <div> 
                        <a data-toggle="tab" href="#new_usuario"><button type="submit" value=""  class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Registrar Usuario</button></a>

                        <a data-toggle="tab" href="#listado"><button type="submit" value=""  class=" btn btn-default "  ><span class="glyphicon glyphicon-print"></span> Reporte Usuarios </button></a>
                    </div>
                    <div class="tab-content">
                        <div id="listado" class="tab-pane fade in active">
                            <div class="box">
                                <div class="box-header" align="center">
                                    <h3 class="box-title">Usuarios</h3>
                                </div><!-- /.box-header -->
                                <br>
                                <div class="box">
                                    <center>
                                        <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                                    </center>
                                </div>
                                <div class="box-body">
                                    <br>
                                    <table class="order-table table">
                                        <thead>
                                            <tr>
                                                <td align="center">Id</td>
                                                <td align="center">Nombre</td>
                                                <td align="center">Apellido Paterno</td>
                                                <td align="center">Apellido Materno</td>
                                                <td align="center">DNI</td>
                                                <td align="center">Telefono</td>
                                                <td align="center">Tipo</td>
                                                <td colspan="2" align="center">Operaciones</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            if (lista != null) {
                                                for (int i = 0; i < lista.size(); i++) {
                                                    Usuario usu = (Usuario) lista.get(i);
                                        %>
                                        <tr><td><% out.println(i + 1); %></td>
                                            <td><% out.println(usu.getNombre()); %></td>
                                            <td><% out.println(usu.getApellidoPaterno()); %></td>
                                            <td><% out.println(usu.getApellidoMaterno()); %></td>
                                            <td><% out.println(usu.getDni()); %></td>
                                            <td><% out.println(usu.getTelefono()); %></td>
                                            <td><% if (usu.getTipo().equals("A")) {
                                                    out.println("Administrador");
                                                }
                                                if (usu.getTipo().equals("V")) {
                                                    out.println("Vendedor");
                                                }
                                                if (usu.getTipo().equals("C")) {
                                                    out.println("Almacenero");
                                                }
                                                %></td>
                                            <td>
                                                <a href="ModificarUsuario.jsp?us_id=<%out.println(usu.getId());%>&us_nom=<%out.println(usu.getNombre());%>&us_apat=<%out.println(usu.getApellidoPaterno());%>&us_amat=<%out.println(usu.getApellidoMaterno());%>&us_dni=<%out.println(usu.getDni());%>&us_tel=<%out.println(usu.getTelefono());%>&us_cue=<%out.println(usu.getCuenta());%>&us_cla=<%out.println(usu.getPassword());%>"><button class="btn btn-block btn-info btn-sm">Modificar</button></a>
                                            </td>
                                            <td>
                                                <a href="sUsuario?opcion=Eliminar&us_id=<%out.println(usu.getId());%>"><button class="btn btn-block btn-danger btn-sm" value="Eliminar" name="opcion">Eliminar</button></a>
                                            </td>
                                            <%}
                                                }%>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body --> 
                        </div>    
                    </div>
                    <div id="new_usuario" class="tab-pane fade">
                        <div class="container">
                            <p><br></p>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-9">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="page-header text-center">
                                                <h1>Registro de Usuario</h1>
                                            </div>
                                            <form name="sUsuario" id="sUsuario" action="sUsuario" method="POST">

                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Nombre</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Apellido Paterno</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="gglyphicon glyphicon-usere"></span></span>
                                                        <input type="text" class="form-control" name="apaterno" id="apaterno" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Apellido Materno</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                        <input type="text" class="form-control" name="amaterno" id="amaterno" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">DNI</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                        <input type="text" class="form-control" name="dni" id="dni" maxlength="8" placeholder="DNI" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Telefono</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone"></span></span>
                                                        <input type="text" class="form-control" name="telefono" maxlength="9" id="telefono" placeholder="Telefono" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Cuenta</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-ok"></span></span>
                                                        <input type="text" class="form-control" name="cuenta" id="cuenta" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Clave</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                        <input type="password" class="form-control" name="clave" id="clave" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Tipo</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-list"></span></span>
                                                        <select class="form-control" name="tipo" id="tipo" aria-describedby="basic-addon1">
                                                            <option>ADMINISTRADOR</option>
                                                            <option>VENDEDOR</option>
                                                            <option>ALMACENERO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="text-center" >
                                                    <button type="submit" class="btn btn-primary" value="Registrar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Registrar </button>
                                                    <button type="submit" class="btn btn-danger" value="Cancelar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
