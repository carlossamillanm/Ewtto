<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Usuario</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>


            <section  class="posts col-md-10" style="background: lightgrey">
                <br>

                <div class="form-group col-md-15" style="border-left:1px solid #ccc;height:200px">
                    <!--<div class="container-fluid" -->
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-heading text-center">
                                        <h1>Modificar Usuario</h1>
                                    </div>
                                    <div>
                                        <form name="sUsuario" id="sUsuario" action="sUsuario" method="POST">
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Id</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                    <input type="text" class="form-control" name="id" id="id" value="<%out.println(request.getParameter("us_id")); %>" placeholder="Nombre" aria-describedby="basic-addon1" required="required" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Nombre</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                <input type="text" class="form-control" name="nombre" id="nombre" value="<%out.println(request.getParameter("us_nom")); %>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Apellido Paterno</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="gglyphicon glyphicon-usere"></span></span>
                                                <input type="text" class="form-control" name="apaterno" id="apaterno" value="<%out.println(request.getParameter("us_apat"));%>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Apellido Materno</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                <input type="text" class="form-control" name="amaterno" id="amaterno" value="<%out.println(request.getParameter("us_amat"));%>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">DNI</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                <input type="text" class="form-control" name="dni" id="dni" value="<%out.println(request.getParameter("us_dni"));%>" placeholder="DNI" maxlength="8" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Telefono</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone"></span></span>
                                                <input type="text" class="form-control" name="telefono" id="telefono" value="<%out.println(request.getParameter("us_tel"));%>" placeholder="Telefono" maxlength="9" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Cuenta</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-ok"></span></span>
                                                <input type="text" class="form-control" name="cuenta" id="cuenta" value="<%out.println(request.getParameter("us_cue"));%>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Clave</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                <input type="password" class="form-control" name="clave" id="clave" value="<%out.println(request.getParameter("us_cla"));%>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Tipo</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-list"></span></span>
                                                <select class="form-control" name="tipo" id="tipo" aria-describedby="basic-addon1">
                                                    <option>ADMINISTRADOR</option>
                                                    <option>VENDEDOR</option>
                                                    <option>ALMACENERO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="text-center form-group col-md-12" > 
                                            <button type="submit" class="btn btn-primary" value="Modificar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Modificar </button>
                                            <button type="submit" class="btn btn-danger" value="Cancelar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
     <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>
