<%@page import="Beans.Usuario"%>
<%@page import="Beans.DetalleVenta"%>
<%@page import="Beans.Producto"%>
<%@page import="Beans.Cliente" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Menu</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <link href="assets/bootstrap/css/datepicker.css" rel="stylesheet">
        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script src="assets/bootstrap/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker();

            });
        </script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="container">
                    <p><br></p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form action="sVenta" name="sVenta" id="sVenta" method="get">
                                        <div class="page-header text-center">
                                            <h1>Registro de Ventas</h1>
                                        </div>
                                        <input type="hidden" class="form-control" name="dni" id="cliente" value="<%if (lista.size() != 0) {
                                                DetalleVenta comprador1 = (DetalleVenta) lista.get(lista.size() - 1);
                                                out.print(comprador1.getVenta().getCliente().getDni());
                                            } else {
                                                out.print(1);
                                            }%>">
                                    <input type="hidden" class="form-control" name="venta" id="cliente" value="<%if (lista.size() != 0) {
                                            DetalleVenta comprador2 = (DetalleVenta) lista.get(lista.size() - 1);
                                            out.print(comprador2.getVenta().getId());
                                        } else {
                                            out.print(1);
                                        }%>">
                                    <table class="table table-bordered col-md-9">
                                        <tbody>
                                            <tr>
                                                <td colspan="5" align="center" ><input type="text" class="form-control" name="cod" id="cod" placeholder="Numero de Guia de Remision" aria-describedby="basic-addon1" required="required"></td>
                                            </tr>
                                            <tr>
                                                <td  align="center">Punto de Partida:</td>
                                                <td colspan="2" align="center"><input type="text" class="form-control" name="pt" id="num" placeholder="Punto de Partida" value="Juan Cuglievan 1435 3er Piso Chiclayo" aria-describedby="basic-addon1" required="required"></td>
                                                <td align="center">Punto de Llegada:</td>
                                                                                      <td colspan="2" align="center"><input type="text" class="form-control" name="pl" id="num" placeholder="Punto de Llegada" value="<%DetalleVenta det1 = (DetalleVenta) lista.get(0);
                                                    out.print(det1.getVenta().getCliente().getDireccion());%>" aria-describedby="basic-addon1" required="required"></td>
                                            </tr>
                                            <tr>
                                                <td align="center">Fecha de emision</td>
                                                <td colspan="2" align="center"> <div class='input-group date' id='datetimepicker'>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span><input type='date' name="fechainicio" id="fecha" class="form-control" />        
                                                    </div></td>
                                                <td align="center">Fecha de Inicio de Traslado</td>
                                                <td colspan="2" align="center"><div class='input-group date' id='datetimepicker'>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span><input type='date' name="fechafin" id="fechafin" class="form-control" />        
                                                    </div></td>

                                            </tr>
                                            <tr>
                                                <td align="center">Razon Social</td>
                                                <td colspan="5" align="center"><input type="text" class="form-control" name="razon" id="num" placeholder="Numero del documento" value="<%
                                                    out.print(det1.getVenta().getCliente().getNombre() + " " + det1.getVenta().getCliente().getApellidoPaterno() + " " + det1.getVenta().getCliente().getApellidoMaterno());
                                                                                      %>" aria-describedby="basic-addon1" required="required"></td>
                                            </tr>
                                            <tr>
                                                <td align="center">CANTIDAD</td>
                                                <td colspan="5" align="center">DESCRIPCION</td>
                                            </tr>
                                            <%
                                                ArrayList<Producto> lProd = (ArrayList<Producto>) session.getAttribute("carrito");
                                                if (lProd != null && lista != null) {
                                                    for (int i = 0; i < lProd.size(); i++) {
                                                        DetalleVenta det = (DetalleVenta) lista.get(i);
                                            %>
                                            <tr><td align="center"><input type="text" class="form-control" name="pre<%out.print(i);%>" id="num" placeholder="Numero del documento" value="<% out.println(det.getCantidad()); %>" aria-describedby="basic-addon1" required="required"></td>
                                                <td colspan="5" align="center"><input type="text" class="form-control" name="des<%out.print(i);%>" id="num" placeholder="Numero del documento" value="<% out.println(det.getProducto().getDescripcion()); %>" aria-describedby="basic-addon1" required="required"></td>
                                                    <%}
                                                        }%>
                                        </tbody>
                                    </table>
                                    <div class="text-center" >
                                        <button type="submit" class="btn btn-primary" value="Remision" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Generar Guia de Remision </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
