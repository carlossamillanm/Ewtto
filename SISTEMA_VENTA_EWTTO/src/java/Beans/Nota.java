package Beans;

public class Nota {
    private Integer id;
    private String Descripcion;
    private Cliente oCliente;

    public Nota() {
    }

    public Nota(Integer id) {
        this.id = id;
    }

    public Nota(Integer id, String Descripcion, Cliente oCliente) {
        this.id = id;
        this.Descripcion = Descripcion;
        this.oCliente = oCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Cliente getoCliente() {
        return oCliente;
    }

    public void setoCliente(Cliente oCliente) {
        this.oCliente = oCliente;
    }

    @Override
    public String toString() {
        return this.Descripcion;
    }
    
    
}
