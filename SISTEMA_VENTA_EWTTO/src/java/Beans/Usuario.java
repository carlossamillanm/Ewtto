package Beans;

public class Usuario {
    private Integer Id;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String Nombre;
    private String Dni;
    private String Telefono;
    private String Cuenta;
    private String Password;
    private String Tipo;

    public Usuario() {
    }

    public Usuario(Integer Id) {
        this.Id = Id;
    }

    public Usuario(Integer Id, String ApellidoPaterno, String ApellidoMaterno, String Nombre, String Dni, String Telefono, String Cuenta, String Password, String Tipo) {
        this.Id = Id;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.Nombre = Nombre;
        this.Dni = Dni;
        this.Telefono = Telefono;
        this.Cuenta = Cuenta;
        this.Password = Password;
        this.Tipo = Tipo;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String Dni) {
        this.Dni = Dni;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getCuenta() {
        return Cuenta;
    }

    public void setCuenta(String Cuenta) {
        this.Cuenta = Cuenta;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }
    
    @Override
    public String toString() {
        return this.Nombre;
    }

    
}
