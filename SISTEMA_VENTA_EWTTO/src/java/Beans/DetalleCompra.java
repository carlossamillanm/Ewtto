package Beans;

public class DetalleCompra {
    Integer Id;
    Integer Cantidad;
    Double Precio;
    Producto Producto;
    Compras Compras;

    public DetalleCompra() {
    }

    public DetalleCompra(Integer Id) {
        this.Id = Id;
    }

    public DetalleCompra(Integer Id, Integer Cantidad, Double Precio, Producto Producto, Compras Compras) {
        this.Id = Id;
        this.Cantidad = Cantidad;
        this.Precio = Precio;
        this.Producto = Producto;
        this.Compras = Compras;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Integer Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double Precio) {
        this.Precio = Precio;
    }

    public Producto getProducto() {
        return Producto;
    }

    public void setProducto(Producto Producto) {
        this.Producto = Producto;
    }

    public Compras getCompras() {
        return Compras;
    }

    public void setCompras(Compras Compras) {
        this.Compras = Compras;
    }
    
    
}
