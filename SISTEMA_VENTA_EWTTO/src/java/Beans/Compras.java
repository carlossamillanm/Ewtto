package Beans;

import java.util.Date;

public class Compras {
    Integer Id;
    String Documento;
    String Numero;
    Date Fecha;

    public Compras() {
    }

    public Compras(Integer Id) {
        this.Id = Id;
    }

    public Compras(Integer Id, String Documento, String Numero, Date Fecha) {
        this.Id = Id;
        this.Documento = Documento;
        this.Numero = Numero;
        this.Fecha = Fecha;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }
    
    
}
