package Beans;

public class Cliente {

    private Integer Id;
    private String Nombre;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String Direccion;
    private String Telefono;
    private String Dni;
    private String Ruc;

    public Cliente() {
    }

    public Cliente(Integer Id) {
        this.Id = Id;
    }

    public Cliente(Integer Id, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String Direccion, String Telefono, String Dni, String Ruc) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.Direccion = Direccion;
        this.Telefono = Telefono;
        this.Dni = Dni;
        this.Ruc = Ruc;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String Dni) {
        this.Dni = Dni;
    }

    public String getRuc() {
        return Ruc;
    }

    public void setRuc(String Ruc) {
        this.Ruc = Ruc;
    }

}
