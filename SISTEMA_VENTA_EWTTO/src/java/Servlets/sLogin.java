package Servlets;

import Beans.Usuario;
import Negocio.nLogin;
import ConexionBD.CADO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "sLogin", urlPatterns = {"/sLogin"})
public class sLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession();
        ArrayList<Usuario> user;
        try {
            nLogin neglogin = new nLogin(new CADO().Conexion());
            String clave = request.getParameter("clave");
            String cuenta = request.getParameter("usuario");

            if (request.getParameter("opcion").equals("Ingresar")) {
                if (neglogin.Validar(cuenta, clave)) {
                    List<Usuario> lista = new ArrayList<Usuario>();
                    if (sesion.getAttribute("user") == null) {
                        user = new ArrayList<Usuario>();
                    } else {
                        user = (ArrayList<Usuario>) sesion.getAttribute("user");
                    }
                    lista=neglogin.User(cuenta, clave);
                    Usuario us=new Usuario();
                    us=(Usuario) lista.get(0);
                    user.add(us);
                    sesion.setAttribute("user", user);
                    request.getRequestDispatcher("MenuPrincipal.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
            }
            if (request.getParameter("opcion").equals("Cerrar")) {
                sesion.invalidate();
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        } catch (Exception e) {
            out.println(e.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
