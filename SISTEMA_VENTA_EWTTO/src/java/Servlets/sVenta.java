package Servlets;

import Beans.Categoria;
import Beans.Cliente;
import Beans.DetalleVenta;
import Beans.Producto;
import Beans.Remision;
import Beans.RemisionProductos;
import Beans.Usuario;
import Beans.Venta;
import Negocio.nCliente;
import Negocio.nProducto;
import Negocio.nRemision;
import Negocio.nVenta;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "sVenta", urlPatterns = {"/sVenta"})
public class sVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nCliente nCl = new nCliente();
            nProducto nProd = new nProducto();
            nVenta nVen = new nVenta();
            nRemision nRen = new nRemision();
            HttpSession sesion = request.getSession();
            ArrayList<Producto> carrito;
            ArrayList<Usuario> user;

            if (request.getParameter("opcion").equals("Registrar")) {
                String nombre = request.getParameter("nombre");
                String Apellidop = request.getParameter("apaterno");
                String Apellidom = request.getParameter("amaterno");
                String Direccion = request.getParameter("direccion");
                String Telefono = request.getParameter("telefono");
                String Dni = request.getParameter("dni");
                String Ruc = request.getParameter("Ruc");
                if (Ruc == null) {
                    Ruc = "";
                }
                Cliente cliente = new Cliente(null, nombre, Apellidop, Apellidom, Direccion, Telefono, Dni, Ruc);

                nCl.Insertar(cliente);

                List<Cliente> lista = new ArrayList<Cliente>();
                lista = nCl.Listar();
                request.setAttribute("listaAsignado", lista);
                request.getRequestDispatcher("Venta.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Buscar")) {
                String dni = request.getParameter("DNI");
                List<Cliente> lista = new ArrayList<Cliente>();
                nCl.Buscar(dni);
                request.setAttribute("listacliente", lista);
            }
            if (request.getParameter("opcion").equals("ListarClientes")) {
                List<Cliente> lista = new ArrayList<Cliente>();
                lista = nCl.Listar();
                request.setAttribute("listacliente", lista);
            }
            if (request.getParameter("opcion").equals("Agregar")) {
                String id = request.getParameter("cli_id");
                String nombre = request.getParameter("cli_nom").toUpperCase();
                String Apellidop = request.getParameter("cli_apat").toUpperCase();
                String Apellidom = request.getParameter("cli_amat").toUpperCase();
                String Direccion = request.getParameter("cli_dir");
                String Telefono = request.getParameter("cli_tel");
                String Dni = request.getParameter("cli_dni");
                String Ruc = request.getParameter("cli_Ruc");
                if (Ruc == null) {
                    Ruc = "";
                }
                Cliente cliente = new Cliente(Integer.parseInt(id), nombre, Apellidop, Apellidom, Direccion, Telefono, Dni, Ruc);

                List<Cliente> lista = new ArrayList<Cliente>();
                lista.add(cliente);
                request.setAttribute("listaAsignado", lista);
                request.getRequestDispatcher("Venta.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("listarProducto")) {
                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
            }
            if (request.getParameter("opcion").equals("Carrito")) {
                String id = request.getParameter("prod_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                if (sesion.getAttribute("carrito") == null) {
                    carrito = new ArrayList<Producto>();
                } else {
                    carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                }
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                if (indice == -1) {
                    carrito.add(pro);
                } else {
                    carrito.set(indice, pro);
                }
                String dni = request.getParameter("cli");
                List<Cliente> lista = new ArrayList<Cliente>();
                lista = nCl.Buscar(dni);
                sesion.setAttribute("carrito", carrito);
                request.setAttribute("listaAsignado", lista);
                request.getRequestDispatcher("Venta.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("RemoverCarrito")) {
                String id = request.getParameter("pro_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                String dni = request.getParameter("cli");
                List<Cliente> lista = new ArrayList<Cliente>();
                lista = nCl.Buscar(dni);
                carrito.remove(indice);
                sesion.setAttribute("carrito", carrito);
                request.setAttribute("listaAsignado", lista);
                request.getRequestDispatcher("Venta.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Venta")) {
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                List<DetalleVenta> Detalle = new ArrayList<DetalleVenta>();
                Double Total = 0.0;
                String num = request.getParameter("num");
                String tipo = request.getParameter("tipo");
                for (int i = 0; i < carrito.size(); i++) {
                    String a = request.getParameter("Precio" + i);
                    Double a1 = Double.parseDouble(a);
                    String b = request.getParameter("Cantidad" + i);
                    Double b1 = Double.parseDouble(b);
                    Double Producto = a1 * b1;
                    Total = Total + Producto;
                }
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    String a = request.getParameter("Precio" + i);
                    Double a1 = Double.parseDouble(a);
                    String b = request.getParameter("Cantidad" + i);
                    Double b1 = Double.parseDouble(b);
                    Venta venta = new Venta(null, null, Total, tipo, num, new Cliente(1), new Usuario(1));
                    DetalleVenta deta = new DetalleVenta(null, Integer.parseInt(request.getParameter("Cantidad" + i)), Double.parseDouble(request.getParameter("Precio" + i)), pr, venta);
                    Detalle.add(deta);
                }
                String pbuscar = request.getParameter("cli");
                List<Cliente> cliente = new ArrayList<Cliente>();
                cliente = nCl.Buscar(pbuscar);
                sesion.setAttribute("carrito", carrito);
                request.setAttribute("lista", Detalle);
                request.setAttribute("listaAsignado", cliente);
                request.getRequestDispatcher("PerfilVenta.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Generar")) {
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                List<DetalleVenta> Detalle = new ArrayList<DetalleVenta>();
                List<Venta> Vent = new ArrayList<Venta>();
                List<Usuario> lista = new ArrayList<Usuario>();
                Usuario us = new Usuario();
                user = (ArrayList<Usuario>) sesion.getAttribute("user");
                us = (Usuario) user.get(0);

                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    String a = request.getParameter("pre" + i);
                    Double a1 = Double.parseDouble(a);
                    String b = request.getParameter("cant" + i);
                    Double b1 = Double.parseDouble(b);
                    Venta venta = new Venta(null, null, Double.parseDouble(request.getParameter("vt")), request.getParameter("tipo"), request.getParameter("cod"), new Cliente(1), new Usuario(us.getId()));
                    DetalleVenta deta = new DetalleVenta(null, Integer.parseInt(request.getParameter("cant" + i)), Double.parseDouble(request.getParameter("pre" + i)), pr, venta);
                    Detalle.add(deta);
                }
                Venta venta = new Venta(null, null, Double.parseDouble(request.getParameter("vt")), request.getParameter("tipo"), request.getParameter("cod"), new Cliente(Integer.parseInt(request.getParameter("cli"))), new Usuario(us.getId()));
                nVen.RegistrarVenta(venta);
                Vent = nVen.Ultima_Venta();
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    DetalleVenta det = Detalle.get(i);
                    Integer Resta = pr.getStock() - det.getCantidad();
                    det.setVenta(Vent.get(0));
                    nVen.RegistrarDetalleVenta(det);
                    nProd.ModificarProducto(new Producto(pr.getId(), pr.getCodigo(), pr.getDescripcion(), pr.getMarca(), pr.getPrecioMenor(), pr.getPrecioMayor(), pr.getPrecioCaja(), pr.getPrecioFactura(), pr.getPrecioCosto(), Resta, pr.getImagen(), new Categoria(pr.getCateg().getId())));
                }
                sesion.invalidate();
                HttpSession sesion1 = request.getSession();
                if (sesion1.getAttribute("user") == null) {
                    user = new ArrayList<Usuario>();
                } else {
                    user = (ArrayList<Usuario>) sesion1.getAttribute("user");
                }
                user.add(us);
                sesion1.setAttribute("user", user);
                request.setAttribute("lista", Detalle);
                if (Vent.get(0).getDocumento().equals("F")) {
                    request.getRequestDispatcher("GuiaRemision.jsp").forward(request, response);
                } else {
                    response.sendRedirect("VentasDia.jsp");
                }

            }
            if (request.getParameter("opcion").equals("Remision")) {
                Usuario us = new Usuario();
                user = (ArrayList<Usuario>) sesion.getAttribute("user");
                us = (Usuario) user.get(0);
                String cod = request.getParameter("cod");
                String id = request.getParameter("venta");
                String pt = request.getParameter("pt");
                String pl = request.getParameter("pl");
                String fi = request.getParameter("fechainicio");
                Date a = Date.valueOf(fi);
                String ff = request.getParameter("fechafin");
                Date b = Date.valueOf(ff);
                String rs = request.getParameter("razon");

                List<DetalleVenta> Detalle = new ArrayList<DetalleVenta>();
                List<Remision> Remision = new ArrayList<Remision>();
                Detalle = nVen.ProductosxVenta(Integer.parseInt(id));
                nRen.RegistrarRemision(new Remision(null, cod, a, b, pt, pl, rs, new Venta(Integer.parseInt(id))));
                Remision = nRen.UltimaRemision();

                for (int i = 0; i < Detalle.size(); i++) {
                    Remision r = new Remision();
                    r = (Remision) Remision.get(0);
                    nRen.RegistrarRemisionxProducto(new RemisionProductos(null, Detalle.get(i).getCantidad(), Detalle.get(i).getPrecio(), new Producto(Detalle.get(i).getProducto().getId()), r));
                }
                sesion.invalidate();
                HttpSession sesion1 = request.getSession();
                if (sesion1.getAttribute("user") == null) {
                    user = new ArrayList<Usuario>();
                } else {
                    user = (ArrayList<Usuario>) sesion1.getAttribute("user");
                }
                user.add(us);
                sesion1.setAttribute("user", user);
                response.sendRedirect("VentasDia.jsp");
            }

        } catch (Exception e) {
            out.print(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
