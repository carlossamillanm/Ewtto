package Negocio;

import Beans.Almacen;
import Beans.Categoria;
import Beans.Producto;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nAlmacen {

    public void RegistrarAlmacen(Almacen obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_almacen(?";
            for (int i = 0; i < 1; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[2];
            param[0] = obean.getCantidad();
            param[1] = obean.getProducto().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void ModificarAlmacen(Almacen obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT actualizar_almacen(?";
            for (int i = 0; i < 2; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[3];
            param[0] = obean.getId();
            param[1] = obean.getCantidad();
            param[2] = obean.getProducto().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void Eliminar(Integer id) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT eliminar_almacen(?)";
            Object param[] = new Object[1];
            param[0] = id;
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Almacen> ListarAlmacen() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Almacen> lista = new ArrayList<Almacen>();
            lista.clear();
            cadena = "SELECT * FROM listar_talmacen()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Almacen objeto = new Almacen();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setCantidad(rs.getInt("cantidad"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"), rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Almacen> BuscarAlmacen(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Almacen> lista = new ArrayList<Almacen>();
            lista.clear();
            cadena = "SELECT * FROM listar_almacen(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Almacen objeto = new Almacen();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setCantidad(rs.getInt("cantidad"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"), rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Almacen> AlmacenxProducto(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Almacen> lista = new ArrayList<Almacen>();
            lista.clear();
            cadena = "SELECT * FROM listar_almacenxprod(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Almacen objeto = new Almacen();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setCantidad(rs.getInt("cantidad"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"), rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
