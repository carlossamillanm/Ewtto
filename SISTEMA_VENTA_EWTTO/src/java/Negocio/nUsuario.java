package Negocio;

import Beans.Usuario;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nUsuario {
    
    public void RegistrarUsuario(Usuario obean) throws Exception{
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_usuario(?";
            for (int i = 0; i < 7; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[8];
            param[0] = obean.getApellidoPaterno().toUpperCase();
            param[1] = obean.getApellidoMaterno().toUpperCase();
            param[2] = obean.getNombre().toUpperCase();
            param[3] = obean.getDni().toUpperCase();
            param[4] = obean.getTelefono();
            param[5] = obean.getCuenta();
            param[6] = obean.getPassword();
            param[7] = obean.getTipo();
            
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
    
    public List<Usuario> ListarUsuarios() throws Exception{
        String cadena = "";
        ResultSet rs;
        try {
            List<Usuario> lista = new ArrayList<Usuario>();
            lista.clear();
            cadena = "SELECT * FROM listar_tusuario()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Usuario objeto=new Usuario();

                objeto.setId(rs.getInt("id"));
                objeto.setApellidoPaterno(rs.getString("apellidop"));
                objeto.setApellidoMaterno(rs.getString("apellidom"));
                objeto.setNombre(rs.getString("nombre"));
                objeto.setDni(rs.getString("dni"));
                objeto.setTelefono(rs.getString("telefono"));
                objeto.setCuenta(rs.getString("cuenta"));
                objeto.setPassword(rs.getString("clave"));
                objeto.setTipo(rs.getString("tipo"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
        public List<Usuario> BuscarUsuarios(Integer id) throws Exception{
        String cadena = "";
        ResultSet rs;
        try {
            List<Usuario> lista = new ArrayList<Usuario>();
            lista.clear();
            cadena = "SELECT * FROM listar_usuario(?)";
             CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0]=id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Usuario objeto=new Usuario();

                objeto.setId(rs.getInt("id"));
                objeto.setApellidoPaterno(rs.getString("apellidop"));
                objeto.setApellidoMaterno(rs.getString("apellidom"));
                objeto.setNombre(rs.getString("nombre"));
                objeto.setDni(rs.getString("dni"));
                objeto.setTelefono(rs.getString("telefono"));
                objeto.setCuenta(rs.getString("cuenta"));
                objeto.setPassword(rs.getString("clave"));
                objeto.setTipo(rs.getString("tipo"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void Eliminar(Integer id) throws Exception{
         String cadena = "";
         CADO ocado = new CADO();
        try {
            cadena = "SELECT eliminar_usuario(?)";
            Object param[] = new Object[1];
            param[0] = id;
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
    
    public void ModificarUsuario(Usuario obean) throws Exception{
        String Cadena = "";
        CADO ocado = new CADO();
        try {
            Cadena = "select actualizar_usuario(?";
            for (int i = 0; i < 8; i++) {
                Cadena = Cadena + ",?";
            }
            Cadena = Cadena + ")";
            Object param[] = new Object[9];
            param[0] = obean.getId();
            param[1] = obean.getApellidoPaterno().toUpperCase();
            param[2] = obean.getApellidoMaterno().toUpperCase();
            param[3] = obean.getNombre().toUpperCase();
            param[4] = obean.getDni();
            param[5] = obean.getTelefono();
            param[6] = obean.getCuenta();
            param[7] = obean.getPassword();
            param[8] = obean.getTipo();
            
            ocado.EjecutaProc(Cadena, param);
            
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
}
