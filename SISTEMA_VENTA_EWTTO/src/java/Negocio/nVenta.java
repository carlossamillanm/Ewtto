package Negocio;

import Beans.Categoria;
import Beans.Cliente;
import Beans.DetalleVenta;
import Beans.Producto;
import Beans.Usuario;
import Beans.Venta;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nVenta {

    public void RegistrarVenta(Venta obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_ventas(?";
            for (int i = 0; i < 4; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[5];
            param[0] = obean.getTotal();
            param[1] = obean.getDocumento().toUpperCase();
            param[2] = obean.getNumero().toUpperCase();
            param[3] = obean.getCliente().getId();
            param[4] = obean.getUsuario().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void RegistrarDetalleVenta(DetalleVenta obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_detalleventa(?";
            for (int i = 0; i < 3; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[4];
            param[0] = obean.getCantidad();
            param[1] = obean.getPrecio();
            param[2] = obean.getProducto().getId();
            param[3] = obean.getVenta().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Venta> ListarVentas() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Venta> lista = new ArrayList<Venta>();
            lista.clear();
            cadena = "SELECT * FROM listar_tventas()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Venta objeto = new Venta();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setTotal(rs.getDouble("total"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                objeto.setCliente(new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")));
                objeto.setUsuario(new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Venta> Ultima_Venta() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Venta> lista = new ArrayList<Venta>();
            lista.clear();
            cadena = "SELECT * FROM ultima_venta()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Venta objeto = new Venta();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setTotal(rs.getDouble("total"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                objeto.setCliente(new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")));
                objeto.setUsuario(new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Venta> BuscarVentasid(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Venta> lista = new ArrayList<Venta>();
            lista.clear();
            cadena = "SELECT * FROM listar_ventas(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Venta objeto = new Venta();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setTotal(rs.getDouble("total"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                objeto.setCliente(new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")));
                objeto.setUsuario(new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DetalleVenta> ListarDetalleVentas() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<DetalleVenta> lista = new ArrayList<DetalleVenta>();
            lista.clear();
            cadena = "SELECT * FROM listar_tdetalleprod()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                DetalleVenta objeto = new DetalleVenta();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cantid"));
                objeto.setPrecio(rs.getDouble("prec"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DetalleVenta> BuscarDetalleVentasid(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<DetalleVenta> lista = new ArrayList<DetalleVenta>();
            lista.clear();
            cadena = "SELECT * FROM listar_detalleprod(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                DetalleVenta objeto = new DetalleVenta();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cantid"));
                objeto.setPrecio(rs.getDouble("prec"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DetalleVenta> ProductosxVenta(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<DetalleVenta> lista = new ArrayList<DetalleVenta>();
            lista.clear();
            cadena = "SELECT * FROM listar_productosxventa(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                DetalleVenta objeto = new DetalleVenta();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cantid"));
                objeto.setPrecio(rs.getDouble("prec"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clientedni")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
      public List<Venta> VentasDia() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Venta> lista = new ArrayList<Venta>();
            lista.clear();
            cadena = "SELECT * FROM listar_ventasdia()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Venta objeto = new Venta();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setTotal(rs.getDouble("total"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                objeto.setCliente(new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"),rs.getString("clienteruc")));
                objeto.setUsuario(new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
