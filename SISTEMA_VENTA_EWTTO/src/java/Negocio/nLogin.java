package Negocio;

import Beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nLogin {

    private Connection connection;

    public nLogin(Connection connection) {
        this.connection = connection;
    }

    public Boolean Validar(String usuario, String clave) throws Exception {
        try {
            String sql = " select tb_usuario_cuenta, tb_usuario_password "
                    + "from tb_usuario where tb_usuario_cuenta ='" + usuario
                    + "' and tb_usuario_password = '" + clave + "';";

            List<Usuario> lista = new ArrayList<Usuario>();
            Usuario objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Usuario();

                objeto.setCuenta(rs.getString("tb_usuario_cuenta"));
                objeto.setPassword(rs.getString("tb_usuario_password"));

                lista.add(objeto);

            }
            if (!lista.isEmpty() && lista.get(0).getCuenta().equals(usuario) && lista.get(0).getPassword().equals(clave)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw e;
        }

    }
    
     public List<Usuario> User(String usuario, String clave) throws Exception {
        try {
            String sql = " select *"
                    + "from tb_usuario where tb_usuario_cuenta ='" + usuario
                    + "' and tb_usuario_password = '" + clave + "';";

            List<Usuario> lista = new ArrayList<Usuario>();
            Usuario objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Usuario();

                objeto.setId(rs.getInt("tb_usuario_id"));
                objeto.setApellidoPaterno(rs.getString("tb_usuario_apellidop"));
                objeto.setApellidoMaterno(rs.getString("tb_usuario_apellidom"));
                objeto.setNombre(rs.getString("tb_usuario_nombre"));
                objeto.setDni(rs.getString("tb_usuario_dni"));
                objeto.setTelefono(rs.getString("tb_usuario_telefono"));
                objeto.setCuenta(rs.getString("tb_usuario_cuenta"));
                objeto.setPassword(rs.getString("tb_usuario_password"));
                objeto.setTipo(rs.getString("tb_usuario_tipo"));

                lista.add(objeto);
            }
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
}
